package com.gbsoft_test;

import android.util.Log;
import android.app.Activity;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import com.gbsoft.gbhealth.GBHealth;

public class GBHealthModule extends ReactContextBaseJavaModule {
    private GBHealth gbHealth;

    GBHealthModule(ReactApplicationContext context) {
        super(context);
        Log.d("GBHealthModule", "GBHealthModule constructor called");
    }

    @Override
    public String getName() {
        return "GBHealth";
    }

    @ReactMethod
    public void setHeightWeight(int height, int weight) {
        Log.d("GBHealthModule", "setHeightWeight called");
        Activity currentActivity = getCurrentActivity();
        if (currentActivity != null) {
            gbHealth = new GBHealth(currentActivity, 15, 8f);
            gbHealth.init();
            gbHealth.setHeightWeight(height, weight);
        }
    }

    @ReactMethod
    public void test(){
        Log.d("GBHealthModule", "test function called");
    }

    @ReactMethod
    public double Get_bmi_() {
        Activity currentActivity = getCurrentActivity();
        if (currentActivity != null) {
            gbHealth = new GBHealth(currentActivity, 15, 8f);
            gbHealth.init();
            gbHealth.setHeightWeight(170, 100);
            return gbHealth.Get_bmi_();
        }
        else{
            return 123;
        }
    }

    @ReactMethod
    public String Get_Library_Version() {
        Activity currentActivity = getCurrentActivity();
        if (currentActivity != null) {
            gbHealth = new GBHealth(currentActivity, 15, 8f);
            gbHealth.init();
            return gbHealth.Get_Library_Version();
        }
        else{
            return "Get_Library_Version";
        }
    }
}
